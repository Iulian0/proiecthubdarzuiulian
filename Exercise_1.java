package Exercises;

import java.util.Arrays;
import java.util.Collection;

public class Exercise_1 {
    public static void main(String[] args) {
        int [] numbers = {90, 80, 70, 60, 50, 40, 30, 20, 10};

        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }
}
